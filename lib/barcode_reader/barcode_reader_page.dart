import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_beep/flutter_beep.dart';
import 'package:qrcode/qrcode.dart';
import 'package:torch_compat/torch_compat.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'dart:math' as Math;

// import 'barcode_reader_overlay_painter.dart';

class BarcodeReaderPage extends StatefulWidget {
  final showBorder;
  final borderFlashDuration;
  final viewfinderWidth;
  final viewfinderHeight;
  final borderRadius;
  final scrimColor;
  final borderColor;
  final borderStrokeWidth;
  final buttonColor;
  final cancelButtonText;
  final successBeep;

  BarcodeReaderPage({
    this.showBorder = true,
    this.borderFlashDuration = 500,
    this.viewfinderWidth = double.infinity,
    this.viewfinderHeight = 50,
    this.borderRadius = 16.0,
    this.scrimColor = Colors.black54,
    this.borderColor = Colors.green,
    this.borderStrokeWidth = 4.0,
    this.buttonColor = Colors.white,
    this.cancelButtonText = "Cancel",
    this.successBeep = true,
  });

  @override
  _BarcodeReaderPageState createState() => _BarcodeReaderPageState();
}

class _BarcodeReaderPageState extends State<BarcodeReaderPage> {
  QRCaptureController _captureController = QRCaptureController();

  bool _hasTorch = false;
  bool _isTorchOn = false;
  bool _isBorderVisible = false;
  Timer _borderFlashTimer;

  @override
  void initState() {
    super.initState();

    if (widget.showBorder) {
      setState(() {
        _isBorderVisible = true;
      });

      if (widget.borderFlashDuration > 0) {
        _borderFlashTimer = Timer.periodic(
            Duration(milliseconds: 500), (timer) {
          setState(() {
            _isBorderVisible = !_isBorderVisible;
          });
        });
      }
    }

    TorchCompat.hasTorch.then((value) {
      setState(() {
        _hasTorch = value;
      });
    });

    _captureController.onCapture((data) {
      _captureController.pause();
      if (widget.successBeep) {
        FlutterBeep.beep();
      }
      Navigator.of(context).pop(data);
    });
  }

  @override
  void dispose() {
    _borderFlashTimer.cancel();
    super.dispose();
  }

  MediaQueryData queryData;

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    return SafeArea(
      child: Scaffold(
        body: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            _buildCaptureView(),
            _buildViewfinder(context),
            _buildAppBar(),
            _buildButtonBar(),
          ],
        ),
      ),
    );
  }

  Widget _buildAppBar() => Align(
    alignment: Alignment.topLeft,
    child: Container(
      color: Colors.black26,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _buildBackButton(),
          _buildTextWidget(),
          SizedBox.shrink()
        ],
      ),
    ),
  );

  Widget _buildTextWidget() => Text("Aponte a câmera para o código de barras",
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          )
  );

  Widget _buildBackButton() => IconButton(
    padding: EdgeInsets.zero,
    icon: Icon(Icons.arrow_back, color: widget.buttonColor,),
    onPressed: (){
      _captureController.pause();
      Navigator.of(context).pop();
    },
  );

  Widget _buildCaptureView() {
    return QRCaptureView(
      controller: _captureController,
    );
  }

  Widget _buildViewfinder(BuildContext context) {
    return CustomPaint(
      size: MediaQuery.of(context).size,
      painter: BarcodeReaderOverlayPainter(
        drawBorder: _isBorderVisible,
        viewfinderWidth: queryData.size.width,
        viewfinderHeight: 150,
        borderRadius: widget.borderRadius,
        scrimColor: widget.scrimColor,
        borderColor: widget.borderColor,
        borderStrokeWidth: widget.borderStrokeWidth,
      ),
      //child:
    );
  }

  Widget _buildButtonBar() {
    return Align(
      alignment: Alignment.bottomRight,
      child: Container(
        color: Colors.black26,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _buildTorchButton(),
            _buildCancelButton(),
          ],
        ),
      ),
    );
  }

  Widget _buildTorchButton() {
    return (_hasTorch)
        ? IconButton(
            icon: Icon(
              (_isTorchOn) ? Icons.flash_on : Icons.flash_off,
              color: widget.buttonColor,
            ),
            onPressed: () {
              if (_isTorchOn) {
                _captureController.torchMode = CaptureTorchMode.off;
              } else {
                _captureController.torchMode = CaptureTorchMode.on;
              }

              setState(() {
                _isTorchOn = !_isTorchOn;
              });
            },
          )
        : Container(
            width: 10,
            height: 10,
          );
  }

  Widget _buildCancelButton() {
    return FlatButton(
      onPressed: () {
        _captureController.pause();
        Navigator.of(context).pop();
      },
      textColor: widget.buttonColor,
      child: Text(widget.cancelButtonText),
    );
  }
}



class BarcodeReaderOverlayPainter extends CustomPainter {
  final drawBorder;
  final viewfinderWidth;
  final viewfinderHeight;
  final borderRadius;
  final scrimColor;
  final borderColor;
  final borderStrokeWidth;

  BarcodeReaderOverlayPainter({
    this.drawBorder = true,
    this.viewfinderWidth = 240.0,
    this.viewfinderHeight = 240.0,
    this.borderRadius = 16.0,
    this.scrimColor = Colors.black54,
    this.borderColor = Colors.green,
    this.borderStrokeWidth = 4.0,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final center = Offset(size.width / 2, size.height / 2);

    final halfCutoutWidth = viewfinderWidth / 2;
    final halfCutoutHeight = viewfinderHeight / 2;

    // Draw the semitransparent area with a cutout in center
    _paintViewfinderScrim(
        canvas, size, center, halfCutoutWidth, halfCutoutHeight);

    // Draw the border arcs in the center cutout corners
    _paintViewfinderBorder(canvas, center, halfCutoutWidth, halfCutoutHeight);
    if (drawBorder) {
      //_paintViewfinderBorder(canvas, center, halfCutoutWidth, halfCutoutHeight);
      _paintViewfinderLine(
          canvas, size, center, halfCutoutWidth, halfCutoutHeight
      );
    }

  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    final previousPainter = oldDelegate as BarcodeReaderOverlayPainter;

    return (previousPainter.drawBorder != drawBorder);
  }

  void _paintViewfinderLine(Canvas canvas, Size canvasSize, Offset center,
      double halfCutoutWidth, double halfCutoutHeight){
    //final scrimPaint = Paint()..color = Colors.blue;
    final p1 = Offset(center.dx - halfCutoutWidth + 15,
      center.dy,
      );
    final p2 = Offset(center.dx + halfCutoutWidth - 15,
      center.dy);
    final paint = Paint()
                    ..color = Colors.red
                    ..strokeWidth = 2;
    canvas.drawLine(p1, p2, paint);
  }

  void _paintViewfinderScrim(Canvas canvas, Size canvasSize, Offset center,
      double halfCutoutWidth, double halfCutoutHeight) {
    final scrimPaint = Paint()..color = scrimColor;

    canvas.drawPath(
        Path.combine(
          PathOperation.difference,
          Path()
            ..addRect(Rect.fromLTWH(0, 0, canvasSize.width, canvasSize.height)),
          Path()
            ..addRRect(RRect.fromLTRBR(
                center.dx - halfCutoutWidth,
                center.dy - halfCutoutHeight,
                center.dx + halfCutoutWidth,
                center.dy + halfCutoutHeight,
                Radius.circular(borderRadius)))
            ..close(),
        ),
        scrimPaint);
  }

  void _paintViewfinderBorder(Canvas canvas, Offset center,
      double halfCutoutWidth, double halfCutoutHeight) {
    final arcAreaSide = borderRadius;
    final arcAreaOffset = borderRadius / 2;

    final borderPaint = Paint()
      ..color = borderColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = borderStrokeWidth;

    // Top left arc
    var arcRect = Rect.fromLTWH(
      center.dx - halfCutoutWidth + arcAreaOffset,
      center.dy - halfCutoutHeight + arcAreaOffset,
      arcAreaSide,
      arcAreaSide,
    );

    canvas.drawPath(
      Path()
        ..addArc(
          arcRect,
          Math.pi,
          Math.pi / 2,
        )
        ..moveTo(arcRect.topRight.dx - arcAreaSide / 2, arcRect.topRight.dy)
        ..relativeLineTo(arcAreaSide, 0)
        ..moveTo(arcRect.bottomLeft.dx, arcRect.bottomLeft.dy - arcAreaSide / 2)
        ..relativeLineTo(0, arcAreaSide),
      borderPaint,
    );

    // Top right arc
    arcRect = Rect.fromLTWH(
      center.dx + halfCutoutWidth - arcAreaSide - arcAreaOffset,
      center.dy - halfCutoutHeight + arcAreaOffset,
      arcAreaSide,
      arcAreaSide,
    );

    canvas.drawPath(
      Path()
        ..addArc(
          arcRect,
          0,
          -Math.pi / 2,
        )
        ..moveTo(arcRect.topLeft.dx - arcAreaSide / 2, arcRect.topLeft.dy)
        ..relativeLineTo(arcAreaSide, 0)
        ..moveTo(
            arcRect.bottomRight.dx, arcRect.bottomRight.dy - arcAreaSide / 2)
        ..relativeLineTo(0, arcAreaSide),
      borderPaint,
    );

    // Bottom right arc
    arcRect = Rect.fromLTWH(
      center.dx + halfCutoutWidth - arcAreaSide - arcAreaOffset,
      center.dy + halfCutoutHeight - arcAreaSide - arcAreaOffset,
      arcAreaSide,
      arcAreaSide,
    );

    canvas.drawPath(
      Path()
        ..addArc(
          arcRect,
          0,
          Math.pi / 2,
        )
        ..moveTo(arcRect.topRight.dx, arcRect.topRight.dy - arcAreaSide / 2)
        ..relativeLineTo(0, arcAreaSide)
        ..moveTo(arcRect.bottomLeft.dx - arcAreaSide / 2, arcRect.bottomLeft.dy)
        ..relativeLineTo(arcAreaSide, 0),
      borderPaint,
    );

    // Bottom left arc
    arcRect = Rect.fromLTWH(
      center.dx - halfCutoutWidth + arcAreaOffset,
      center.dy + halfCutoutHeight - arcAreaSide - arcAreaOffset,
      arcAreaSide,
      arcAreaSide,
    );

    canvas.drawPath(
      Path()
        ..addArc(
          arcRect,
          Math.pi,
          -Math.pi / 2,
        )
        ..moveTo(arcRect.topLeft.dx, arcRect.topLeft.dy - arcAreaSide / 2)
        ..relativeLineTo(0, arcAreaSide)
        ..moveTo(
            arcRect.bottomRight.dx - arcAreaSide / 2, arcRect.bottomRight.dy)
        ..relativeLineTo(arcAreaSide, 0),
      borderPaint,
    );
  }
}
